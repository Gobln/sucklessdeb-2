/* Bar functionality */
#include "bar_indicators.c"
#include "bar_tagicons.c"
#include "bar.c"

#include "bar_alpha.c"
#include "bar_anybar.c"

/* Other patches */
#include "alttab.c"
#include "attachx.c"
#include "autostart.c"
#include "cyclelayouts.c"
#include "focusfollowmouse.c"
#include "fullscreen.c"
#include "ipc.c"
#ifdef VERSION
#include "ipc/IPCClient.c"
#include "ipc/yajl_dumps.c"
#include "ipc/ipc.c"
#include "ipc/util.c"
#endif
#include "restartsig.c"
#include "rotatestack.c"
#include "scratchpad.c"
#include "swallow.c"
/* Layouts */
#include "layout_facts.c"
#include "layout_monocle.c"
#include "layout_tile.c"

