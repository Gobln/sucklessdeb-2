
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/goblin/git/polybar-dwm/lib/dwmipcpp/src/connection.cpp" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/connection.cpp.o" "gcc" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/connection.cpp.o.d"
  "/home/goblin/git/polybar-dwm/lib/dwmipcpp/src/errors.cpp" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/errors.cpp.o" "gcc" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/errors.cpp.o.d"
  "/home/goblin/git/polybar-dwm/lib/dwmipcpp/src/packet.cpp" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/packet.cpp.o" "gcc" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/packet.cpp.o.d"
  "/home/goblin/git/polybar-dwm/lib/dwmipcpp/src/types.cpp" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/types.cpp.o" "gcc" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/types.cpp.o.d"
  "/home/goblin/git/polybar-dwm/lib/dwmipcpp/src/util.cpp" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/util.cpp.o" "gcc" "lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/src/util.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
